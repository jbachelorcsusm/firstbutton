# Classroom Code, 2018.01.24: FirstButton #

Building on our knowledge, this app is now slightly interactive: We can track how many attendees enter an event venue. Cool!

Exercise:
1. Add a new button to track when guests leave the venue under the current button.
2. Add an event-handler to that button.
3. In event-handler for the new button, add code to decrement the guest count and update the label.

### Questions? ###

* Always always always: If you have a question, someone else in class almost certainly has that same question, so please do ask!
* [Post questions on Cougar Courses](https://cc.csusm.edu/mod/oublog/view.php?id=630348)