﻿using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace FirstButton
{
    //[XamlCompilation(XamlCompilationOptions.Skip)]   // Un-comment this line to DISABLE xaml compilation for this particular view.
    //[XamlCompilation(XamlCompilationOptions.Compile)]   // Un-comment this line to ENABLE xaml compilation for this particular view.
    public partial class FirstButtonPage : ContentPage
    {
        int attendeeCounter = 0;

        public FirstButtonPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(FirstButtonPage)}:  constructor");
            InitializeComponent();
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Handle_Clicked)}:  Adding attendee number {++attendeeCounter}");
            AttendeeCounterLabel.Text = $"{attendeeCounter} guests";
        }
    }
}
